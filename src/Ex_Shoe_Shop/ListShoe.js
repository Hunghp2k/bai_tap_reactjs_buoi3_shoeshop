import React, { Component } from 'react'
import ItemShoe from './ItemShoe'

export default class ListShoe extends Component {
    render() {
        return (
            <div>
                <h2>ListShoe</h2>
                <div className='row'>
                    {this.props.list.map((item) => {
                        return <ItemShoe handleAddToCart={this.props.handleAdd} shoe={item} />;
                    })}
                </div>
            </div>
        );
    }
}
