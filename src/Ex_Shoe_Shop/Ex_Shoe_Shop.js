import React, { Component } from 'react'
import CartShoe from './CartShoe'
import { data_shoe } from './data_shoe'
import ListShoe from './ListShoe'

export default class Ex_Shoe_Shop extends Component {
    state = {
        listShoe: data_shoe,
        cart: [],
    }
    handleAdd = (shoe) => {
        let cloneCart = [...this.state.cart];
        let index = cloneCart.findIndex((item) => {
            return item.id == shoe.id;
        })
        if (index == -1) {
            let newCart = { ...shoe, soLuong: 1 }
            cloneCart.push(newCart);
        } else {
            cloneCart[index].soLuong += 1;
        }
        this.setState({
            cart: cloneCart,
        })
    }
    handleDelete = (idshoe) => {
        let newCart = this.state.cart.filter((item) => {
            return item.id != idshoe
        })
        this.setState({ cart: newCart });
    }
    handleQuantity = (idShoe, luachon) => {
        let cloneCart = [...this.state.cart]
        let index = cloneCart.findIndex((item) => {
            return item.id == idShoe;
        });
        cloneCart[index].soLuong = cloneCart[index].soLuong + luachon;
        this.setState({
            cart: cloneCart
        })
    }
    render() {
        return (
            <div >
                <h2>Ex_Shoe_Shop</h2>
                {this.state.cart.length > 0 && <CartShoe handleQuantity={this.handleQuantity} handleDelete={this.handleDelete} cart={this.state.cart} />}
                <ListShoe handleAdd={this.handleAdd} list={this.state.listShoe} />
            </div>
        )
    }
}
